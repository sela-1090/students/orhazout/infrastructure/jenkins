# Implement Jenkins using HELM

## Prerequisites
1. kubernetes cluster
2. helm chart


### Add a Helm repository named "jenkins"
```
helm repo add jenkins https://charts.jenkins.io
```


### updates the local cache of available Helm charts from all added repositories
```
helm repo update
```


### upgrade a Helm release named "jenkins"
```
helm upgrade --install jenkins jenkins/jenkins --namespace cicd -f values.yaml
```


### Retrieve the value of the "jenkins-admin-password" key from the "myjenkins" secret in the "cicd" namespace.
```
kubectl get secret myjenkins --namespace cicd -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode
```


### Set up a port forwarding between your local machine's port 8080.
```
kubectl --namespace cicd port-forward svc/jenkins 8080:8080
```

### connect to jenkins server in adress: http//localhost:8080 && login with the admin user and password from previews step.




### Add users:


1. Log in to Jenkins using the existing administrator credentials.

2. Once you are logged in, click on "Manage Jenkins" in the Jenkins home page.

3. In the Manage Jenkins page, click on "Manage Users" from the available options.

4. Click on the "Create User" button to create a new user.

5. Fill in the user details for the first additional admin user:



Clean UP:
```
helm uninstall myjenkins --namespace cicd
```
